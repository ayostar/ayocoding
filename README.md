## AYO CODING

This is a hub which includes all FreeCodeCamp projects required for the front-end certificate. <br>
[Coded entirely in React]

# Libraries used

* bulma 0.6.1
* concurrently 3.5.1
* moment 2.20.1
* jsonp 0.2.1
* node-sass-chokidar 0.0.3
* react 16.2.0
* react-dom 16.2.0
* react-router-dom 4.2.2
* react-transition-group 2.2.1
* react-scripts 1.0.17
* react-twitter-widgets 1.7.1

# How to run:

* git clone https://gitlab.com/ayostar/ayocoding.git
* cd ayocoding
* npm install
* npm run dev (will run on localhost:3000)
