import React from 'react';

const CalculatorButton = ({ title, onClick, disabled, css }) => {
  return (
    <button
      className={`button is-outlined is-large calculator-button ${css}`}
      onClick={onClick}
      disabled={disabled ? true : false}>
      {title}
    </button>
  );
};

export default CalculatorButton;
