import React, { Component } from 'react';

import CalculatorButton from './CalculatorButton';

class Calculator extends Component {
  state = {
    display: '0',
    isCalculated: false
  };

  reset() {
    this.setState({
      display: '0',
      isCalculated: false
    });
  }

  pressed(number) {
    const { display, isCalculated } = this.state;
    let newDisplay = '';
    if (typeof number === 'number') {
      if (isCalculated) {
        newDisplay = parseInt(number, 10);
        this.setState({ isCalculated: false });
      } else {
        newDisplay = display === '0' ? parseInt(number, 10) : display.toString() + number;
      }
    } else {
      if (isCalculated) {
        this.setState({ isCalculated: false });
      }
      switch (number) {
        case '+':
          newDisplay = display + ' + ';
          break;
        case '-':
          newDisplay = display + ' - ';
          break;
        case '*':
          newDisplay = display + ' * ';
          break;
        case '/':
          newDisplay = display + ' / ';
          break;
        default:
          console.log('default');
      }
    }

    this.setState({
      display: newDisplay
    });
  }

  getResult(a, b, op) {
    switch (op) {
      case '+':
        return a + b;
      case '-':
        return a - b;
      case '*':
        return a * b;
      case '/':
        return a / b;
      default:
        return 0;
    }
  }

  calculate() {
    const { display } = this.state;

    function findNasobeniDeleni(item) {
      return item === '*' || item === '/';
    }

    let arr = display.split(' ');

    /* prohledej rovnici pro vyskyt nasobeni/deleni, ktere ma prednost */
    while (arr.findIndex(findNasobeniDeleni) !== -1) {
      const index = arr.findIndex(findNasobeniDeleni);
      const result = this.getResult(+arr[index - 1], +arr[index + 1], arr[index]);
      arr.splice(index - 1, 3);
      arr.splice(index - 1, 0, result);
    }

    /* secti celou rovnici */
    while (arr.length > 1) {
      let mezisoucet = this.getResult(+arr[0], +arr[2], arr[1]);
      arr.splice(0, 3);
      arr.unshift('' + mezisoucet);
    }

    this.setState({
      display: arr,
      isCalculated: true
    });
  }

  render() {
    const { display } = this.state;
    return (
      <section className="section">
        <div className="container">
          <h2 className="project-name title has-text-centered is-size-1">Calculator</h2>
          <div className="calculator-container message">
            <div className="message-body" style={{ padding: '2rem' }}>
              <div className="calculator-row">
                <div className="display">
                  <h3 className="title is-3 display-content">{display}</h3>
                </div>
              </div>
              <div className="calculator-row">
                <CalculatorButton title="" onClick={null} disabled />
                <CalculatorButton title="" onClick={null} disabled />
                <CalculatorButton title="C" onClick={() => this.reset()} />
                <CalculatorButton title="/" onClick={() => this.pressed('/')} />
              </div>
              <div className="calculator-row">
                <CalculatorButton title={7} onClick={() => this.pressed(7)} />
                <CalculatorButton title={8} onClick={() => this.pressed(8)} />
                <CalculatorButton title={9} onClick={() => this.pressed(9)} />
                <CalculatorButton title="*" onClick={() => this.pressed('*')} />
              </div>
              <div className="calculator-row">
                <CalculatorButton title={4} onClick={() => this.pressed(4)} />
                <CalculatorButton title={5} onClick={() => this.pressed(5)} />
                <CalculatorButton title={6} onClick={() => this.pressed(6)} />
                <CalculatorButton title="-" onClick={() => this.pressed('-')} />
              </div>
              <div className="calculator-row">
                <CalculatorButton title={1} onClick={() => this.pressed(1)} />
                <CalculatorButton title={2} onClick={() => this.pressed(2)} />
                <CalculatorButton title={3} onClick={() => this.pressed(3)} />
                <CalculatorButton title="+" onClick={() => this.pressed('+')} />
              </div>
              <div className="calculator-row">
                <CalculatorButton title="+-" onClick={() => this.pressed('+-')} />
                <CalculatorButton title={0} onClick={() => this.pressed(0)} />
                <CalculatorButton title="." onClick={() => this.pressed('.')} />
                <CalculatorButton title="=" onClick={() => this.calculate()} css="is-primary" />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Calculator;
