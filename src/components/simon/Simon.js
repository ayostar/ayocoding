import React, { Component } from 'react';
import Switch from 'react-switch';
import Sound from 'react-sound';

class Simon extends Component {
  state = {
    intervalID: null,
    engine: false,
    status: false,
    strict: false,
    message: '',
    round: 0,
    waitingResponse: false,
    wrongInput: false,
    response: [],
    sequence: [],
    currPosSequence: 0,
    colorHighlight: null,
    playSound: null
  };

  /* make sure to clear any intervals upon leaving component */
  componentWillUnmount() {
    const { intervalID } = this.state;
    clearInterval(intervalID);
  }

  /* function to start the game */
  start = message => {
    this.setState({ status: true, message, round: 1 }, () => {
      this.generateRandom();
      this.showSequence();
    });
  };

  /* clear the game and stop it */
  stop = message => {
    const { intervalID } = this.state;
    clearInterval(intervalID);
    this.setState({
      status: false,
      message,
      round: 0,
      sequence: [],
      response: [],
      intervalID: null,
      currPosSequence: 0,
      waitingResponse: false,
      playSound: null
    });
  };

  /* show computer generated sequence of colors, which is basically interval */
  showSequence = () => {
    this.setState({
      intervalID: setInterval(() => this.timer(), 1000)
    });
  };

  /* function to run every second while called by showSequence fx
    when it reaches end, it will clear interval */
  timer = () => {
    const { currPosSequence, sequence, intervalID } = this.state;
    if (currPosSequence < sequence.length) {
      this.highlightColor(sequence[currPosSequence]);
      this.setState({ currPosSequence: currPosSequence + 1 });
    } else {
      clearInterval(intervalID);
      this.setState({ intervalID: null, waitingResponse: true });
    }
  };

  highlightColor = num => {
    this.setState({ colorHighlight: num, waitingResponse: false, playSound: num });
    setTimeout(() => {
      this.setState({ colorHighlight: null, waitingResponse: true, playSound: null });
    }, 600);
  };

  generateRandom = () => {
    const { sequence } = this.state;
    const random = Math.floor(Math.random() * Math.floor(4));
    this.setState({
      sequence: [...sequence, random]
    });
  };

  handleClick = num => {
    const { waitingResponse, sequence, response, round, strict } = this.state;
    const currResponseIndex = response.length;

    /* check response */
    if (waitingResponse && sequence.length > currResponseIndex) {
      // logic for correct answer
      if (sequence[currResponseIndex] === num) {
        this.setState({ response: [...response, num] }, () => {
          if (this.state.response.length === sequence.length) {
            this.setState({ response: [], currPosSequence: 0 }, () =>
              setTimeout(() => {
                this.generateRandom();
                this.showSequence();
                this.setState({ round: round + 1 });
              }, 1000)
            );
          }
        });
      } else {
        // logic for wrong answer
        this.setState({ wrongInput: true, playSound: 4 });
        setTimeout(() => {
          this.setState({ wrongInput: false, response: [], currPosSequence: 0, playSound: null });
          if (strict) {
            this.stop();
            this.start();
          } else {
            this.showSequence();
          }
        }, 1000);
      }
    }
    this.highlightColor(num);
  };

  handleChange = () => {
    const { engine } = this.state;
    if (engine) {
      this.stop();
    }
    this.setState({ engine: !engine });
  };

  handleStart = () => {
    const { engine, status } = this.state;
    if (engine) {
      if (status) {
        this.stop();
        this.start('pressed start while ON');
      } else {
        this.start('normal start');
      }
    }
  };

  displayScore = () => {
    // engine ? (!status ? '--' : round < 9 ? `0${round}` : round) : ''
    /* I think nested ternary condition is too much in this case */
    const { engine, status, round, wrongInput } = this.state;
    if (engine) {
      if (!status) {
        return '--';
      } else {
        if (round < 9) {
          return wrongInput ? '!!' : `0${round}`;
        } else {
          return wrongInput ? '!!' : round;
        }
      }
    }
    return '';
  };

  render() {
    const { status, colorHighlight, playSound, waitingResponse, engine, strict } = this.state;
    return (
      <section className="section">
        <div className="container">
          <h2 className="project-name title has-text-centered is-size-1">Simon Game</h2>
          <div className="message">
            <div className="message-body">
              <div className="simon-container">
                <div
                  className={colorHighlight === 0 ? 'green-light' : 'green'}
                  onClick={() => waitingResponse && this.handleClick(0)}
                  style={{ cursor: waitingResponse && 'pointer' }}>
                  {playSound === 0 && (
                    <Sound url="../sounds/0.mp3" playStatus={Sound.status.PLAYING} />
                  )}
                </div>
                <div
                  className={colorHighlight === 1 ? 'red-light' : 'red'}
                  onClick={() => waitingResponse && this.handleClick(1)}
                  style={{ cursor: waitingResponse && 'pointer' }}>
                  {playSound === 1 && (
                    <Sound url="../sounds/1.mp3" playStatus={Sound.status.PLAYING} />
                  )}
                </div>
                <div
                  className={colorHighlight === 3 ? 'yellow-light' : 'yellow'}
                  onClick={() => waitingResponse && this.handleClick(3)}
                  style={{ cursor: waitingResponse && 'pointer' }}>
                  {playSound === 3 && (
                    <Sound url="../sounds/3.mp3" playStatus={Sound.status.PLAYING} />
                  )}
                </div>
                <div
                  className={colorHighlight === 2 ? 'blue-light' : 'blue'}
                  onClick={() => waitingResponse && this.handleClick(2)}
                  style={{ cursor: waitingResponse && 'pointer' }}>
                  {playSound === 2 && (
                    <Sound url="../sounds/2.mp3" playStatus={Sound.status.PLAYING} />
                  )}
                </div>
                <div className="control-container">
                  <h1 className="is-size-1 control-title">SIMON</h1>
                  <div className="control-buttons-container">
                    <div>
                      <div className="display">
                        <h3 className="is-size-3 has-text-centered">{this.displayScore()}</h3>
                      </div>
                      <h5 className="is-size-6">Score</h5>
                      {playSound === 4 && (
                        <Sound url="../sounds/wrong.mp3" playStatus={Sound.status.PLAYING} />
                      )}
                    </div>
                    <div>
                      <button
                        className="control-button"
                        onClick={() => this.handleStart()}
                        style={{ backgroundColor: status ? '#f00' : '#520000' }}
                      />
                      <h5 className="is-size-6">Start</h5>
                    </div>
                    <div>
                      <button
                        className="control-button"
                        onClick={() => this.setState({ strict: !strict })}
                        style={{ backgroundColor: strict ? '#ff0' : '#FFBA55' }}
                      />
                      <h5 className="is-size-6">Strict</h5>
                    </div>
                  </div>
                  <div className="control-powerbutton-container">
                    <label htmlFor="engine-switch">
                      <Switch
                        width={70}
                        height={28}
                        checked={engine}
                        onChange={this.handleChange}
                        id="engine-switch"
                        uncheckedIcon={<div className="uncheckedIcon">Off</div>}
                        checkedIcon={<div className="checkedIcon">On</div>}
                      />
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Simon;
