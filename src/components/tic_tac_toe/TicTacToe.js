import React, { Component } from 'react';

const winPositions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

class TicTacToe extends Component {
  constructor() {
    super();
    this.state = {
      playerID: 1,
      isPlayer1Turn: true,
      arr: Array(9).fill(0),
      win: false,
      console: ''
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleSelectClick(id) {
    this.setState({
      playerID: id
    });
  }

  handleClick(id, playerID) {
    const nextState = this.state.arr;

    if (nextState[id] === 0) {
      nextState[id] = playerID;
    }

    console.log(nextState);

    this.setState({
      arr: nextState
    });

    this.handleWin();
  }

  renderCell(id) {
    const val = this.state.arr[id];
    if (val === 1) {
      return <i className="fa fa-circle-o circle" />;
    } else if (val === 2) {
      return <i className="fa fa-times cross" />;
    }

    return null;
  }

  handleWin = () => {
    if (this.calculateWin()) {
      this.setState({
        arr: Array(9).fill(0),
        win: true,
        console: 'YOU FUCKEN WIN!!! YEE..'
      });
    }
  };

  calculateWin() {
    const grid = this.state.arr;
    const wp = winPositions;

    for (let i = 0; i < winPositions.length; i++) {
      if (grid[wp[i][0]] !== 0 && grid[wp[i][1]] !== 0 && grid[wp[i][2]] !== 0) {
        return true;
      }
    }

    return false;
  }

  render() {
    const player = this.state.playerID;

    return (
      <div className="container">
        <div className="console">{this.state.console}</div>
        <div className="players">
          <button onClick={() => this.handleSelectClick(1)}>Player 1: O</button>
          <button onClick={() => this.handleSelectClick(2)}>Player 2: X</button>
          <button onClick={() => this.handleWin()}>Calculate</button>
        </div>
        <div className="ttt-container">
          <div className="ttt-row">
            <div className="ttt-cell top-left" onClick={() => this.handleClick(0, player)}>
              {this.renderCell(0)}
            </div>
            <div className="ttt-cell top-middle" onClick={() => this.handleClick(1, player)}>
              {this.renderCell(1)}
            </div>
            <div className="ttt-cell top-right" onClick={() => this.handleClick(2, player)}>
              {this.renderCell(2)}
            </div>
          </div>
          <div className="ttt-row">
            <div className="ttt-cell middle-left" onClick={() => this.handleClick(3, player)}>
              {this.renderCell(3)}
            </div>
            <div className="ttt-cell middle-middle" onClick={() => this.handleClick(4, player)}>
              {this.renderCell(4)}
            </div>
            <div className="ttt-cell middle-right" onClick={() => this.handleClick(5, player)}>
              {this.renderCell(5)}
            </div>
          </div>
          <div className="ttt-row">
            <div className="ttt-cell bottom-left" onClick={() => this.handleClick(6, player)}>
              {this.renderCell(6)}
            </div>
            <div className="ttt-cell bottom-middle" onClick={() => this.handleClick(7, player)}>
              {this.renderCell(7)}
            </div>
            <div className="ttt-cell bottom-right" onClick={() => this.handleClick(8, player)}>
              {this.renderCell(8)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TicTacToe;
