import React from 'react';

const Tribute = () => {
  return (
    <div className="container">
      <h1>Bud Spencer</h1>
      <span>(Carlo Pedersoli)</span>
      <p className="lead">Big guy who prefered good ol' style hit first and think later...</p>
      <img
        className="img-fluid tribute-img"
        src="http://image.toutlecine.com/photos/a/l/a/aladdin-1986-03-g.jpg"
        alt="Bud Spencer as Aladin"
      />
      <ul className="list-group">
        <h3 className="movielist-heading text-center">A list of his movies:</h3>
        <h1 className="pt-2 mb-0">
          <span className="badge badge-info">60'</span>
        </h1>
        <li className="list-group-item">
          <strong className="date">1959</strong>- Hannibal
        </li>
        <li className="list-group-item">
          <strong className="date">1967</strong>- God Forgives... I Don't! (first time as Bud
          Spencer together with Terence Hill)
        </li>
        <li className="list-group-item">
          <strong className="date">1968</strong>- Ace High
        </li>
        <li className="list-group-item">
          <strong className="date">1969</strong>- Boot Hill
        </li>
        <h1 className="pt-3 mb-0">
          <span className="badge badge-info">70'</span>
        </h1>
        <li className="list-group-item">
          <strong className="date">1970</strong>- Call Me Trinity
        </li>
        <li className="list-group-item">
          <strong className="date">1971</strong>- Blackie the Pirate{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1971</strong>- Trinity Is Still My Name{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1972</strong>- All the Way, Boys{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1974</strong>- Watch Out, We're Mad{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1975</strong>- Two Missionaries{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1976</strong>- Crime Busters{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1978</strong>- Odds and Evens{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1979</strong>- I'm For the Hippopotamus{' '}
        </li>
        <h1 className="pt-3 mb-0">
          <span className="badge badge-info">80'</span>
        </h1>
        <li className="list-group-item">
          <strong className="date">1981</strong>- Who Finds a Friend Finds a Treasure{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1983</strong>- Go For It!{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1984</strong>- Double Trouble{' '}
        </li>
        <li className="list-group-item">
          <strong className="date">1985</strong>- Miami Supercops{' '}
        </li>
        <h1 className="pt-3 mb-0">
          <span className="badge badge-info">90'</span>
        </h1>
        <li className="list-group-item">
          <strong className="date">1994</strong>- Troublemakers{' '}
        </li>
      </ul>
      <hr />
      <div className="alert alert-success text-center">
        <strong>Heads up!</strong> If you want to find out more information about this awesome actor
        please&nbsp;
        <a
          href="https://en.wikipedia.org/wiki/Bud_Spencer"
          target="_blank"
          rel="noopener noreferrer">
          visit his wiki page
        </a>.
      </div>
    </div>
  );
};

export default Tribute;
