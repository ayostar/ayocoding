import React from 'react';

const Loading = () => {
  return (
    <li className="twitch-item message has-text-centered">
      <div className="message-body">
        <h3 className="twitch-name">Loading...</h3>
      </div>
    </li>
  );
};

export default Loading;
