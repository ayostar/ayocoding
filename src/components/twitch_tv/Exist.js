import React from 'react';

const Exist = ({ avatar, bio, name, status, stream }) => {
  return (
    <li className={status !== 'offline' ? 'twitch-item message is-success' : 'twitch-item message'}>
      <div className="message-body">
        <div className="columns is-vcentered">
          <div className="column is-one-quarter">
            <div className="twitch-avatar-container">
              <img className="twitch-avatar rounded-circle" src={avatar} alt={name} />
              <h3 className="title is-4 twitch-name">
                <a href={`https://www.twitch.tv/${name}`} rel="noopener noreferrer" target="_blank">
                  {name}
                </a>
              </h3>
            </div>
          </div>
          <div className="column">
            <h3 className="is-6">Bio:</h3>
            <span className="twitch-bio">{bio}</span>
          </div>
          <div className="column">
            <div className="twitch-status">
              {stream ? <img src={stream} alt="" /> : null}
              {status !== 'offline' ? (
                <a
                  className="twitch-status-text"
                  rel="noopener noreferrer"
                  target="_blank"
                  href={`https://www.twitch.tv/${name}`}>
                  {status}
                  <span className="icon">
                    <i className="fa fa-external-link" />
                  </span>
                </a>
              ) : (
                status
              )}
            </div>
          </div>
        </div>
      </div>
    </li>
  );
};

export default Exist;
