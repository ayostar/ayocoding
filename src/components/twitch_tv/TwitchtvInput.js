import React from 'react';

const TwitchTvInput = ({ value, onChange, onSubmit }) => {
  return (
    <form className="is-horizontal" onSubmit={onSubmit}>
      <div className="field has-addons">
        <div className="control">
          <input type="text" className="input is-large" value={value} onChange={onChange} />
        </div>
        <div className="control">
          <button className="button is-primary is-large" type="submit">
            Add channel
          </button>
        </div>
      </div>
    </form>
  );
};

export default TwitchTvInput;
