import React, { Component } from 'react';
import jsonp from 'jsonp';
import twitchStreams from '../../data/twitchStreams.json';

import TwitchtvInput from './TwitchtvInput';
import Loading from './Loading';
import Exist from './Exist';
import NExist from './NExist';

const STATUS_URL = 'https://wind-bow.gomix.me/twitch-api/streams/';
const AVATAR_URL = 'https://wind-bow.gomix.me/twitch-api/users/';

class Twitchtv extends Component {
  state = {
    streams: twitchStreams,
    activeTab: 1,
    value: ''
  };

  componentDidMount() {
    this.state.streams.forEach((item, index) => {
      this.fetchStreamData(index);
    });
  }

  fetchStreamData(number) {
    let modifiedStreams = this.state.streams;
    let currentStream = modifiedStreams[number];
    jsonp(`${AVATAR_URL}${currentStream.name}`, null, (err, res) => {
      const { bio, logo } = res;
      if (!err) {
        currentStream.bio = bio;
        currentStream.avatar = logo;
        currentStream.is = res.hasOwnProperty('error') ? false : true;
      } else {
        console.log(err.message);
      }
      this.setState({
        streams: modifiedStreams
      });
    });
    jsonp(`${STATUS_URL}${currentStream.name}`, null, (err, res) => {
      const { stream } = res;
      if (!err) {
        currentStream.stream = res.stream && res.stream.preview.small;
        currentStream.status = stream === null ? 'offline' : stream.channel.status;
      } else {
        console.log(err);
      }
      this.setState({
        streams: modifiedStreams
      });
    });
  }

  toggleTabs(num) {
    if (num !== this.state.activeTab) {
      this.setState({ activeTab: num });
    }
  }

  renderChannels(num) {
    const { streams } = this.state;
    let result = [];
    switch (num) {
      case 2:
        result = streams.filter(item => item.status !== 'offline' && item.is);
        break;
      case 3:
        result = streams.filter(item => item.status === 'offline' && item.is);
        break;
      default:
        result = streams;
    }
    return result.map((item, index) => {
      const { avatar, bio, name, status, stream } = item;
      return !item.hasOwnProperty('is') ? (
        <Loading key={index} />
      ) : item.is ? (
        <Exist key={index} avatar={avatar} bio={bio} name={name} status={status} stream={stream} />
      ) : (
        <NExist key={index} name={name} />
      );
    });
  }

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    if (this.state.value !== '') {
      this.setState(
        {
          streams: [...this.state.streams, { name: this.state.value }],
          value: ''
        },
        () => this.fetchStreamData(this.state.streams.length - 1)
      );
    }
  };

  render() {
    const { activeTab, value } = this.state;
    return (
      <section className="section">
        <div className="container">
          <h2 className="project-name title has-text-centered is-size-1">TwitchTV</h2>
          <div className="columns is-gapless is-vcentered">
            <div className="column is-half">
              <TwitchtvInput
                value={value}
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
              />
            </div>
            <div className="column">
              <div className="tabs is-toggle is-large is-right is-primary">
                <ul>
                  <li className={activeTab === 1 ? 'is-active' : null}>
                    <a onClick={() => this.toggleTabs(1)}>
                      <span>All</span>
                    </a>
                  </li>
                  <li className={activeTab === 2 ? 'is-active' : null}>
                    <a onClick={() => this.toggleTabs(2)}>
                      <span>Online</span>
                    </a>
                  </li>
                  <li className={activeTab === 3 ? 'is-active' : null}>
                    <a onClick={() => this.toggleTabs(3)}>
                      <span>Offline</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <ul className="channels-container">{this.renderChannels(activeTab)}</ul>
        </div>
      </section>
    );
  }
}

export default Twitchtv;
