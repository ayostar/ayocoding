import React from 'react';

const NExist = ({ name }) => {
  return (
    <li className="twitch-item message has-text-centered is-danger">
      <div className="message-body">
        <div className="columns is-vcentered">
          <div className="column is-one-quarter">
            <div className="twitch-avatar-container">
              <img className="twitch-avatar" src="../img/cross.png" alt="" />
            </div>
          </div>
          <div className="column">
            <h3 className="twitch-name">{name}'s account is either deleted or non-existant!</h3>
          </div>
        </div>
      </div>
    </li>
  );
};

export default NExist;
