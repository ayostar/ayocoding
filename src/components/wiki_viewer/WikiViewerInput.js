import React from 'react';

const WikiViewerInput = ({ value, onChange, onSubmit }) => {
  return (
    <form className="form wiki-form" onSubmit={onSubmit}>
      <div className="field has-addons">
        <div className="control ">
          <input
            autoFocus
            type="text"
            value={value}
            onChange={onChange}
            className="input wiki-input is-large"
          />
        </div>
        <div className="control">
          <button className="button is-primary is-large" type="submit">
            <span className="icon is-right">
              <i className="fa fa-search" />
            </span>
          </button>
        </div>
      </div>
    </form>
  );
};

export default WikiViewerInput;
