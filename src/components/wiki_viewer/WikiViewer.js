import React, { Component } from 'react';
import jsonp from 'jsonp';

import WikiViewerInput from './WikiViewerInput';

/* TODO: add indicator to show loading */

const url = `https://en.wikipedia.org/w/api.php?action=query&format=json&generator=search&gsrlimit=10&prop=extracts&exintro&explaintext&exsentences=1&gsrsearch=`;

class WikiViewer extends Component {
  state = {
    value: '',
    list: []
  };

  fetchData() {
    jsonp(url + this.state.value, null, (err, res) => {
      if (err) {
        console.log(err);
      } else {
        this.setState({ list: res.query.pages });
      }
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    if (this.state.value !== '') {
      this.fetchData();
    }
  };

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  render() {
    const { list, value } = this.state;
    const keys = Object.keys(list);

    const resultList = keys.map((item, key) => {
      const { title } = list[parseInt(item, 10)];
      return (
        <a
          key={key}
          className="result-link"
          rel="noopener noreferrer"
          target="_blank"
          href={`https://en.wikipedia.org/wiki/${title}`}>
          <div className="result-item message">
            <div className="message-body">
              <h3 className="result-title title is-size-3">
                {title}
                <span className="icon">
                  <i className="result-icon fa fa-external-link" />
                </span>
              </h3>
              <p className="result-desc">{list[parseInt(item, 10)].extract}</p>
            </div>
          </div>
        </a>
      );
    });

    return (
      <section className="section">
        <div className="container">
          <h2 className="project-name title is-size-1 has-text-centered">WikiViewer</h2>
          <div className="wikiviewer-container">
            <WikiViewerInput
              value={value}
              onChange={this.handleChange}
              onSubmit={this.handleSubmit}
            />
          </div>
          <div className="wiki-random">
            <a
              className="button is-primary is-outlined is-medium"
              rel="noopener noreferrer"
              target="_blank"
              href="https://en.wikipedia.org/wiki/Special:Random">
              <span>I'm feeling lucky today..</span>
              <span className="icon is-small">
                <i className="fa fa-external-link" />
              </span>
            </a>
          </div>
          <div className="result-group">{resultList}</div>
        </div>
      </section>
    );
  }
}

export default WikiViewer;
