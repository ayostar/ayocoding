import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { basic, intermediate, advanced } from '../../data/projectsData.json';

import HeaderGroup from './HeaderGroup';

class Header extends Component {
  constructor() {
    super();
    this.state = { isOpen: false };
  }

  handleToggle = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  componentWillReceiveProps() {
    this.handleToggle();
  }

  render() {
    const { isOpen } = this.state;
    const burgerCSS = 'button navbar-burger is-primary';
    const menuCSS = 'navbar-menu';

    return (
      <nav className="navbar is-primary has-shadow">
        <div className="container">
          <div className="navbar-brand">
            <Link to="/" className="navbar-item">
              <img src="../img/logo.png" className="foto" alt="foto" />
            </Link>
            <button
              className={isOpen ? `${burgerCSS} is-active` : `${burgerCSS}`}
              onClick={this.handleToggle}
            >
              <span />
              <span />
              <span />
            </button>
          </div>
          <div className={isOpen ? `${menuCSS} is-active` : `${menuCSS}`}>
            <div className="navbar-end">
              <HeaderGroup heading="Basic" arr={basic} />
              <HeaderGroup heading="Intermediate" arr={intermediate} />
              <HeaderGroup heading="Advanced" arr={advanced} />
              <a
                className="navbar-item"
                href="https://gitlab.com/ayostar/ayocoding"
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon">
                  <i className="fa fa-lg fa-gitlab" />
                </span>
              </a>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
