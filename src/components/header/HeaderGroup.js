import React from 'react';
import { NavLink } from 'react-router-dom';

const HeaderGroup = ({ heading, arr }) => {
  return (
    <div className="navbar-item has-dropdown is-hoverable">
      <a className="navbar-link">{heading}</a>
      <div className="navbar-dropdown">
        {arr.map((item, index) => {
          const { url, title } = item;
          return (
            <NavLink to={url} key={index} className="navbar-item" activeClassName="is-active">
              {title}
            </NavLink>
          );
        })}
      </div>
    </div>
  );
};

export default HeaderGroup;
