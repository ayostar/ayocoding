import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './header/Header';
import Landing from './landing/Landing';
import Calculator from './calculator/Calculator';
import LocalWeather from './local_weather/LocalWeather';
import Pomodoro from './pomodoro/Pomodoro';
import Portfolio from './portfolio/Portfolio';
import RandomQuote from './random_quote/RandomQuote';
import TicTacToe from './tic_tac_toe/TicTacToe';
import Tribute from './tribute/Tribute';
import Twitchtv from './twitch_tv/Twitchtv';
import WikiViewer from './wiki_viewer/WikiViewer';
import Simon from './simon/Simon';

/* TODO: Refactor entire app */
/* TODO: refactor all components into smaller pieces to see if any are reusable */

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container-fluid">
          <Route component={ScrollToTop} />
          <Route path="*" component={Header} />
          <Switch>
            <Route path="/" exact component={Landing} />
            <Route path="/tribute" component={Tribute} />
            <Route path="/portfolio" component={Portfolio} />
            <Route path="/localweather" component={LocalWeather} />
            <Route path="/randomquote" component={RandomQuote} />
            <Route path="/wikiviewer" component={WikiViewer} />
            <Route path="/twitchtv" component={Twitchtv} />
            <Route path="/calculator" component={Calculator} />
            <Route path="/pomodoro" component={Pomodoro} />
            <Route path="/tictactoe" component={TicTacToe} />
            <Route path="/simon" component={Simon} />
          </Switch>
        </div>
      </Router>
    );
  }
}

const ScrollToTop = () => {
  window.scrollTo(0, 0);
  return null;
};

export default App;
