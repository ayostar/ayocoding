import React, { Component } from 'react';
import moment from 'moment';
import jsonp from 'jsonp';

const APIURL = 'http://api.openweathermap.org/data/2.5/weather';
const IMGURL = 'http://openweathermap.org/img/w/';
const APPID = '4ece8c5b965a073e18c2bf4c42f0c66a';

class LocalWeather extends Component {
  state = {
    location: '',
    temp: '',
    metric: true,
    icon: '',
    lat: 0,
    long: 0,
    isLoaded: false
  };

  changeDegrees = () => {
    const { metric, temp } = this.state;
    const deg = metric ? temp * (9 / 5) + 32 : (temp - 32) * (5 / 9);
    this.setState({
      temp: deg.toFixed(1),
      metric: !metric
    });
  };

  getCoords = ({ coords }) => {
    const { latitude, longitude } = coords;
    this.setState({
      lat: latitude,
      long: longitude
    });
    this.fetchData();
  };

  fetchData = () => {
    const { lat, long } = this.state;
    jsonp(`${APIURL}?lat=${lat}&lon=${long}&APPID=${APPID}`, null, (err, res) => {
      const temp = (res.main.temp - 273.15).toFixed(1);
      const location = res.name;
      const icon = res.weather[0].icon;
      this.setState({
        location: location,
        temp: temp,
        icon: icon,
        isLoaded: true
      });
    });
  };

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(this.getCoords);
  }

  render() {
    const { icon, location, metric, temp, isLoaded } = this.state;
    const today = moment().format('DD. MMMM YYYY');
    const Loading = () => <div>weather is loading...</div>;
    return (
      <section className="section">
        <div className="container">
          <h2 className="project-name title has-text-centered is-size-1">Local Weather</h2>
          <div className="project-container message">
            <div className="message-body">
              <h2 className="subtitle is-3 has-text-centered">
                Today is: <b>{today}</b>
              </h2>
              <button
                className="button is-danger is-outlined is-pulled-right is-cleared"
                onClick={this.changeDegrees}>
                &deg;{metric ? 'F' : 'C'}
              </button>
              {isLoaded ?
                (<div className="weather-container">
                <img
                  className="image weather-icon"
                  src={icon === '' ? '/' : `${IMGURL}${icon}.png`}
                  alt="weather icon"
                />
                <h2 className="title is-1 weather-temp">
                  {temp} &deg;{metric ? 'C' : 'F'}
                </h2>
              </div>) : <Loading />}
              <h2 className="title is-1 has-text-centered weather-location">{location}</h2>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default LocalWeather;
