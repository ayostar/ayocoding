import React from 'react';

const Portfolio = () => {
  return (
    <div className="container">
      <div className="portfolio-container">
        <h1>Hi, I'm Martin Franek</h1>
        <h3>Front-end developer from Prague, Czech Republic</h3>
      </div>
    </div>
  );
};

export default Portfolio;
