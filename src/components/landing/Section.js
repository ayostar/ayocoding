import React from 'react';
import Project from './Project';

const Section = ({ color, title, desc, projects }) => {
  return (
    <section className={`hero ${color}`}>
      <div className="hero-body">
        <div className="container">
          <h1 className="title">{title}</h1>
          <h2 className="subtitle">{desc}</h2>
          <div className="columns">
            {projects.map((item, index) => {
              const { url, title, img, desc } = item;
              return <Project key={index} url={url} title={title} img={img} desc={desc} />;
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Section;
