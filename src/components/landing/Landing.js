import React from 'react';
import Section from './Section';

import { basic, intermediate, advanced } from '../../data/projectsData.json';

const Landing = () => {
  return (
    <div>
      <Section title="Basic Projects" desc="very simple projects" projects={basic} />
      <Section
        title="Intermediate Projects"
        desc="mostly playing with API's kind of projects"
        projects={intermediate}
      />
      <Section title="Advanced Projects" desc="mostly games" projects={advanced} />
    </div>
  );
};

export default Landing;
