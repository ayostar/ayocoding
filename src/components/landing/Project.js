import React from 'react';
import { Link } from 'react-router-dom';

const Project = ({ url, title, img, desc }) => {
  return (
    <div className="column project-column">
      <Link to={`${url}`} className="card">
        <div className="card-image">
          <img className="image project-img" src={img} alt="" />
          <div className="img-overlay">
            <span className="img-overlay-text is-size-1 is-primary">{title}</span>
          </div>
        </div>
        <div className="card-content">
          <span className="is-2">{desc}</span>
        </div>
      </Link>
    </div>
  );
};

export default Project;
