import React, { Component } from 'react';

// import Progress from './Progress';
import Zkouska from './Zkouska';

class Pomodoro extends Component {
  state = {
    intervalID: null,
    workTime: 10,
    breakTime: 1,
    workClock: 60,
    maxWorkClock: 60,
    breakClock: 10,
    maxBreakClock: 10,
    running: false,
    breakRunning: false
  };

  componentWillUnmount() {
    const { intervalID } = this.state;
    clearInterval(intervalID);
  }

  secondsToHMS(value) {
    const min = Math.floor((value % 3600) / 60);
    const sec = Math.floor((value % 3600) % 60);
    return (min <= 9 ? '0' + min : min) + ':' + (sec <= 9 ? '0' + sec : sec);
  }

  plus = arg => {
    const { workTime, breakTime, intervalID, running } = this.state;
    if (arg === 'work') {
      const nextState = workTime + 1;
      this.setState({
        workTime: nextState,
        workClock: nextState * 60,
        maxWorkClock: nextState * 60
      });
    } else {
      const nextState = breakTime + 1;
      this.setState({
        breakTime: nextState,
        breakClock: nextState * 60,
        maxBreakClock: nextState * 60
      });
    }
    if (!intervalID) {
      clearInterval(intervalID);
      this.setState({
        intervalID: false
      });
    }
    if (running) {
      this.setState({
        running: false
      });
    }
  };

  minus = arg => {
    const { workTime, breakTime, intervalID } = this.state;
    if (arg === 'work') {
      if (workTime > 1) {
        const nextState = workTime - 1;
        this.setState({
          workTime: nextState,
          workClock: nextState * 60,
          maxWorkClock: nextState * 60
        });
      }
    } else {
      if (breakTime > 1) {
        const nextState = breakTime - 1;
        this.setState({
          breakTime: nextState,
          breakClock: nextState * 60,
          maxBreakClock: nextState * 60
        });
      }
    }
    clearInterval(intervalID);
    this.setState({
      intervalID: false,
      running: false
    });
  };

  timer = () => {
    const { workClock, breakRunning, breakClock, running } = this.state;
    if (workClock === 0 && !breakRunning) {
      this.setState({
        breakRunning: !breakRunning
      });
    } else if (breakClock === 0) {
      this.setState({
        running: !running,
        breakRunning: !breakRunning
      });
      this.reset();
    } else {
      if (running) {
        if (breakRunning) {
          this.setState({
            breakClock: breakClock - 1
          });
        } else {
          this.setState({
            workClock: workClock - 1
          });
        }
      }
    }
  };

  reset() {
    clearInterval(this.state.intervalID);
    this.setState({
      intervalID: false
    });
  }

  handleClock = () => {
    const { running, workClock, breakClock } = this.state;
    if (!running && workClock !== 0 && breakClock !== 0) {
      this.setState({
        running: true
      });
      const intervalID = setInterval(this.timer, 1000);
      this.setState({
        intervalID: intervalID
      });
    } else {
      this.reset();
      this.setState({
        running: false
      });
    }
  };

  render() {
    const { workClock, breakClock, maxWorkClock, maxBreakClock } = this.state;
    const workWidth = workClock / maxWorkClock * 100;
    const breakWidth = breakClock / maxBreakClock * 100 * 5 + 'px';
    console.log(workWidth);

    return (
      <section className="section">
        <div className="container">
          <h3 className="project-name title has-text-centered is-size-1">Pomodoro clock</h3>
          <div className="project-container message">
            <div className="message-body">
              <div className="pomodoro-set">
                <h3 className="pomodoro-set-title">
                  Work time: {this.state.workTime} <span className="pomodoro-units">min</span>
                </h3>
                <button
                  className="button is-primary is-outlined pomodoro-button"
                  onClick={() => this.plus('work')}>
                  +
                </button>
                <button
                  className="button is-danger is-outlined pomodoro-button"
                  onClick={() => this.minus('work')}>
                  -
                </button>
              </div>
              <div className="pomodoro-set">
                <h3 className="pomodoro-set-title">
                  Break time: {this.state.breakTime} <span className="pomodoro-units">min</span>
                </h3>
                <button
                  className="button is-primary is-outlined pomodoro-button"
                  onClick={() => this.plus('break')}>
                  +
                </button>
                <button
                  className="button is-danger is-outlined pomodoro-button"
                  onClick={() => this.minus('break')}>
                  -
                </button>
              </div>
              {/* <Progress color="" value={} />
              <Progress color="" value={} /> */}
              <div className="pomodoro-clock-container">
                <div className="pomodoro-clock">
                  <div
                    className="pomodoro-progressBar progressBar-work"
                    style={{ width: workWidth }}
                    onClick={() => this.handleClock()}>
                    <div className="pomodoro-progressBar-number">
                      {this.secondsToHMS(workClock)}
                    </div>
                  </div>
                </div>
              </div>
              <div className="pomodoro-clock-container">
                <div className="pomodoro-clock">
                  <div
                    className="pomodoro-progressBar progressBar-break"
                    style={{ width: breakWidth, backgroundColor: '#0b0' }}>
                    <div className="pomodoro-progressBar-number">
                      {this.secondsToHMS(breakClock)}
                    </div>
                  </div>
                </div>
              </div>
              <Zkouska height={`${workWidth}%`} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Pomodoro;
