import React from 'react';

const Progress = ({ width, workClock, color }) => {
  return (
    <div className="pomodoro-clock-container">
      <div className="pomodoro-clock">
        <div className="pomodoro-progressBar progressBar-work" style={{ width: width }}>
          <div className="pomodoro-progressBar-number">{this.secondsToHMS(workClock)}</div>
        </div>
      </div>
    </div>
  );
};

export default Progress;
