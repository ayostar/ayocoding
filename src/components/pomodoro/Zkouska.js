import React from 'react';

const Zkouska = ({ height }) => {
  return (
    <div className="ball-container">
      <span className="time">02:00</span>
      <div className="filling" style={{ height: height }} />
    </div>
  );
};

export default Zkouska;
