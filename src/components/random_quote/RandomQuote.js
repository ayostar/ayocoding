import React, { Component } from 'react';
import { Share } from 'react-twitter-widgets';
import { CSSTransition } from 'react-transition-group';
import Mottos from '../../data/Mottos.json';

/* TODO: refactor those terrible if statements */

class RandomQuote extends Component {
  state = {
    number: 0,
    motto: '',
    show: false,
    numbersArray: []
  };

  componentWillMount() {
    this.generateMotto();
    this.setState({ show: false });
  }

  generateMotto() {
    function generateRandom() {
      return Math.floor(Math.random() * Mottos.length);
    }
    const { number } = this.state;
    let random = 0;

    do {
      random = generateRandom();
    } while (number === random);

    const array = this.calculateNumbersArray(random);
    this.setState({
      number: random,
      numbersArray: array,
      motto: Mottos[random],
      show: true
    });
  }

  selectMotto(num, symbol) {
    let result = num + symbol;
    if (result === -2 && symbol === -2) {
      result = 35;
    }
    if ((result === -1 && symbol === -2) || (result === -1 && symbol === -1)) {
      result = 36;
    }
    if (result === 38 && symbol === 2) {
      result = 1;
    }
    if ((result === 37 && symbol === 1) || (result === 37 && symbol === 2)) {
      result = 0;
    }
    const array = this.calculateNumbersArray(result);
    this.setState({
      number: result,
      numbersArray: array,
      motto: Mottos[result],
      show: true
    });
  }

  calculateNumbersArray(num) {
    let newArray = [];
    for (let i = -2; i < 3; i++) {
      let result = num + i;
      switch (i) {
        case -2:
          if (result === -1) {
            result = 36;
          } else if (result === -2) {
            result = 35;
          }
          break;
        case -1:
          if (result === -1) {
            result = 36;
          }
          break;
        case 1:
          if (result === 37) {
            result = 0;
          }
          break;
        case 2:
          if (result === 37) {
            result = 0;
          } else if (result === 38) {
            result = 1;
          }
          break;
        default:
          break;
      }
      newArray.push(result);
    }
    return newArray;
  }

  render() {
    const { number, motto, show, numbersArray } = this.state;
    return (
      <section className="section">
        <div className="container">
          <h2 className="project-name title has-text-centered is-size-1">Random Quote</h2>
          <div className="project-container message">
            <div className="message-body">
              <div id="twitter" className="motto-tweet is-pulled-right">
                <Share url="/" options={{ text: motto }} />
              </div>
              <CSSTransition
                in={show}
                timeout={300}
                classNames="fade"
                onEntered={() => this.setState({ show: false })}>
                <div className="motto-text">
                  <h3 className="is-size-4">"{motto}"</h3>
                </div>
              </CSSTransition>
              <div className="has-text-centered">
                <a className="button is-primary" onClick={() => this.generateMotto()}>
                  Generate Funny Motto
                </a>
              </div>
            </div>
          </div>
          <div className="numbers-container">
            <div className="arrow has-text-centered">
              <span className="icon is-large has-text-primary">
                <i className="fa fa-3x fa-angle-down" />
              </span>
            </div>
            <div className="columns is-marginless">
              <div
                className="column is-one-fifth has-text-centered message numbers-div"
                onClick={() => this.selectMotto(number, -2)}>
                <h1 className="is-size-1 message-body numbers-message">{numbersArray[0] + 1}</h1>
              </div>
              <div
                className="column is-one-fifth has-text-centered message numbers-div"
                onClick={() => this.selectMotto(number, -1)}>
                <h1 className="is-size-1 message-body numbers-message">{numbersArray[1] + 1}</h1>
              </div>
              <div className="column is-one-fifth has-text-centered message is-primary">
                <h1 className="is-size-1 message-body numbers-current">{numbersArray[2] + 1}</h1>
              </div>
              <div
                className="column is-one-fifth has-text-centered message numbers-div"
                onClick={() => this.selectMotto(number, 1)}>
                <h1 className="is-size-1 message-body numbers-message">{numbersArray[3] + 1}</h1>
              </div>
              <div
                className="column is-one-fifth has-text-centered message numbers-div"
                onClick={() => this.selectMotto(number, 2)}>
                <h1 className="is-size-1 message-body numbers-message">{numbersArray[4] + 1}</h1>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default RandomQuote;
